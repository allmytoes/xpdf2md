import logging

from xpdf2md.document import (
    ChapterType,
    Document,
    Chapter,
    FencedText,
    Paragraph,
    Image,
    SpanStyle,
)

logger = logging.getLogger("xpdf2md")


class MdWriter:
    def __init__(
            self,
            document: Document
        ) -> None:
        self.document = document
        logger.info("--------------------------------------------")
        logger.info("Markdown generation starting")
        logger.info("--------------------------------------------")
        self._set_chap_file_names()
        self._write()
    
    def _set_chap_file_names(self):
        self._chap_file_names = []
        headless_count = 0
        blind_count = 0
        normal_count = 0
        for ix, chap in enumerate(self.document.chapters):
            if chap.chapter_type == ChapterType.NORMAL:
                normal_count += 1
                self._chap_file_names.append(
                    "c{:03}n{:03}".format(ix + 1, normal_count)
                )
            elif chap.chapter_type == ChapterType.BLIND:
                blind_count += 1
                self._chap_file_names.append(
                    "c{:03}b{:03}".format(ix + 1, blind_count)
                )
            elif chap.chapter_type == ChapterType.HEADLESS:
                headless_count += 1
                assert blind_count == 0
                assert normal_count == 0
                self._chap_file_names.append(
                    "a{:03}h{:03}".format(ix + 1, headless_count)
                )
            else:
                assert False

    @staticmethod
    def pre_post_for_style(style: SpanStyle) -> tuple[str, str]:
        if style == SpanStyle.CODE:
            return "`", "`"
        if style == SpanStyle.EMPH:
            return "_", "_"
        if style == SpanStyle.HEAVYEMPH:
            return "**", "**"
        assert False

    def _get_chap_string(self, chapter: Chapter) -> str:
        lines: list[str] = []
        if chapter.chapter_type != ChapterType.HEADLESS:
            lines.append("# {}\n".format(chapter.title.replace("\n", " - ")))
        for section in chapter.sections:
            if isinstance(section, FencedText):
                lines.append("\n```")
                lines.append(section.text)
                lines.append("```\n")
            elif isinstance(section, Image):
                lines.append("\n<img src=\"{}\"/>\n".format(section.url))
            elif isinstance(section, Paragraph):
                parts: list[str] = []
                for span in section.spans:
                    if span.style == SpanStyle.NORMAL:
                        parts.append(span.text)
                    else:
                        if len(span.text) > 0:
                            s_pre = " " if span.text[0] == " " else ""
                            s_post = " " if span.text[-1] == " " else ""
                            f_pre, f_post = MdWriter.pre_post_for_style(span.style)
                            t = span.text.strip()
                            parts.append("{}{}{}{}{}".format(
                                s_pre,
                                f_pre,
                                t,
                                f_post,
                                s_post
                            ))
                p_text = "".join(parts)
                next_line = ""
                for word in p_text.split():
                    if len(word) <= 80 and len(next_line) + len(word) > 80:
                        lines.append(next_line)
                        next_line = ""
                    else:
                        if len(next_line) > 0:
                            next_line += " "
                    next_line += word
                if len(next_line) > 0:
                    lines.append(next_line)
                lines.append("")
            else:
                assert False
        return "\n".join(lines)

    def _write(self):
        for chapter, file_name in zip(self.document.chapters, self._chap_file_names):
            logger.debug("Writing chapter {}...".format(chapter.title))
            chap_str = self._get_chap_string(chapter)
            with open(file_name + ".md", "w") as outfile:
                outfile.write(chap_str)
        logger.debug("Writing contents index...")
        content = "# Contents\n\n<div class=\"contents\"><ol>"
        for ix, chapter in enumerate(self.document.chapters):
            pandoc_file_name = "ch{:03}.xhtml".format(
                ix + 2  # starts with 1, + 1 for the Contents chapter to be added
            )
            if chapter.chapter_type == ChapterType.NORMAL:
                content += "\n<li><a href='{}'>{}</a></li>".format(
                    pandoc_file_name,
                    chapter.title,
                )
                indexed = True
            else:
                indexed = False
            logger.info(
                "Chap {:03} | {:8} | {:8} | {:11} | {} ".format(
                    ix + 1,
                    chapter.chapter_type.name,
                    self._chap_file_names[ix],
                    pandoc_file_name if indexed else "     -     ",
                    chapter.title,
                )
            )
        content += "\n</ol></div>\n"
        with open("b_contents.md", "w") as outfile:
            outfile.write(content)

