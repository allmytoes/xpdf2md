import logging
import lxml.etree as Et
from typing import Optional

from itertools import chain
from xpdf2md.errors import XMLParseError

Element = Et._Element
Tree = Et.ElementTree

logger = logging.getLogger("xpdf2md")

def stringify_inner(node) -> str:
    parts = (
        [node.text]
        +
        list(
            chain(*([
                #c.text,
                Et.tostring(c, encoding="utf8", with_tail=False).decode("utf8"),
                c.tail
            ] for c in node.getchildren()))
        )
        #+
        #[node.tail]
    )
    return ''.join(filter(None, parts))


class FontProfile:
    def __init__(
            self,
            id: int,
            size: int,
            family: str,
            color: str,
        ) -> None:
        self.id = id
        self.size = size
        self.color = color
        self.family = family
        self.number_total = 0
        self.number_lines = 0

    def __str__(self) -> str:
        return "[fonspec {} ({}:{}/{})]".format(
            self.id,
            self.size,
            self.family,
            self.color,
        )


class PageElement:
    def __init__(
            self,
            x: int,
            y: int,
            width: int,
            height: int,
        ) -> None:
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def __repr__(self) -> str:
        return self.__str__()
    

class TextElement(PageElement):
    def __init__(
            self,
            x: int,
            y: int,
            width: int,
            height: int,
            font_id: int,
            text: str,
        ) -> None:
        super().__init__(x, y, width, height)
        self.font_id = font_id
        self.text = text

    def __str__(self):
        return "[Text {}x{} @ {} {} / {}]".format(
            self.width,
            self.height,
            self.x,
            self.y,
            self.font_id
        )


class ImageElement(PageElement):
    def __init__(
            self,
            x: int,
            y: int,
            width: int,
            height: int,
            path: str,
        ) -> None:
        super().__init__(x, y, width, height)
        self.path = path

    def __str__(self):
        return "[Image {}x{} @ {} {} / {}]".format(
            self.width,
            self.height,
            self.x,
            self.y,
            self.path
        )


class ElementLine:
    def __init__(self, elements: list[PageElement]) -> None:
        self.elements = elements


class Page:
    def __init__(self, elements: list[PageElement], page_num: int) -> None:
        self.page_num = page_num
        assert len(elements) > 0
        elements = sorted(elements, key=lambda e: e.x)
        elements.sort(key=lambda e: e.y)
        self.lines: list[ElementLine] = []
        line_elements: list[PageElement] = []
        last_y: Optional[int] = None
        for element in elements:
            if last_y is None or element.y == last_y:
                line_elements.append(element)
                if last_y is None:
                    last_y = element.y
            else:
                assert last_y is not None and last_y < element.y
                self.lines.append(ElementLine(line_elements))
                line_elements = [element]
                last_y = element.y
        assert len(line_elements) > 0
        self.lines.append(ElementLine(line_elements))


class Parser:
    def __init__(self, root: Element) -> None:
        self.root  = root
        self.font_profiles_by_id: dict[int, FontProfile] = {}
        self.pages: list[Page] = []

        self._parse()

    def _parse(self):
        for e_page in self.root.findall("./page"):
            try:
                page_num = int(e_page.attrib["number"])
            except AttributeError as e:
                raise XMLParseError("Missing page attribute ({})".format(str(e)))
            except ValueError as e:
                raise XMLParseError("Page attribute invalid ({})".format(str(e)))
            logger.debug("Parsing page {}".format(page_num))
            self._parse_fontspecs(e_page)
            self.pages.append(self._parse_content(e_page, page_num))
    

    def _parse_content(self, e_page: Element, page_num: int) -> Page:
        elements: list[PageElement] = []
        for e_text in e_page.findall("./text"):
            try:
                x = int(e_text.attrib["left"])
                y = int(e_text.attrib["top"])
                width = int(e_text.attrib["width"])
                height = int(e_text.attrib["height"])
                font_id = int(e_text.attrib["font"])
                text = stringify_inner(e_text)
                elements.append(TextElement(
                    x=x,
                    y=y,
                    width=width,
                    height=height,
                    font_id=font_id,
                    text=text,
                ))
            except AttributeError as e:
                raise XMLParseError("Missing text attribute ({})".format(str(e)))
            except ValueError as e:
                raise XMLParseError("Text attribute invalid ({})".format(str(e)))
        for e_text in e_page.findall("./image"):
            try:
                x = int(e_text.attrib["top"])
                y = int(e_text.attrib["left"])
                width = int(e_text.attrib["width"])
                height = int(e_text.attrib["height"])
                path = e_text.attrib["src"]
                elements.append(ImageElement(
                    x=x,
                    y=y,
                    width=width,
                    height=height,
                    path=path,
                ))
            except AttributeError as e:
                raise XMLParseError("Missing image attribute ({})".format(str(e)))
            except ValueError as e:
                raise XMLParseError("Image attribute invalid ({})".format(str(e)))
        p = Page(elements, page_num)
        logger.debug(" → Added page with {} lines".format(len(p.lines)))
        return p


    def _parse_fontspecs(self, page: Element):
        for e_spec in page.findall("./fontspec"):
            assert isinstance(e_spec, Element)
            try:
                id = int(e_spec.attrib["id"])
                size = int(e_spec.attrib["size"])
                family = e_spec.attrib["family"]
                color = e_spec.attrib["color"]
            except AttributeError as e:
                raise XMLParseError("Missing fontspec attribute ({})".format(str(e)))
            except ValueError as e:
                raise XMLParseError("Fontspec attribute invalid ({})".format(str(e)))
            assert id not in self.font_profiles_by_id
            #if id in self.font_profiles_by_id:
            #    p = self.font_profiles_by_id[id]
            #    assert p.id == id
            #    assert p.size == size
            #    assert p.family == family
            #    assert p.color == color
            self.font_profiles_by_id[id] = FontProfile(
                id=id,
                size=size,
                family=family,
                color=color,
            )
            logger.debug(" → Added fontspec {}".format(str(self.font_profiles_by_id[id])))



