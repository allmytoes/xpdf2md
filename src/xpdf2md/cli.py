#!/usr/bin/env python
import argparse
import logging
from os import stat_result
import sys
import lxml.etree as Et
from typing import Optional
from xpdf2md.assembler import Assembler


from xpdf2md.logging_formatter import CustomFormatter
from xpdf2md.errors import FinalXPDF2MDException, ReturnCode
from xpdf2md.parser import Parser
from xpdf2md.stat import Stat
from xpdf2md.mdwriter import MdWriter

Element = Et._Element
Tree = Et.ElementTree

logger = logging.getLogger("xpdf2md")
handler = logging.StreamHandler()
formatter = CustomFormatter()
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def profile(in_args):
    with open(in_args.pdfxml_infile[0], "r") as infile:
        root: Element = Et.fromstring(bytes(infile.read(), encoding='utf8'))
        parser = Parser(root)
        stat = Stat(parser.pages, parser.font_profiles_by_id)
        assembler = Assembler(
            pages=parser.pages,
            font_profiles=parser.font_profiles_by_id,
            stat_result= stat.result
        )
        MdWriter(document=assembler._doc)
        print("Stats: \n" + str(stat.result) + "\n")
        print("Line-end hyphens: ")
        for e in assembler.hypen_breaks:
            print("  " + e)


def _setup_profile_parser(p: argparse.ArgumentParser) -> None:
    p.set_defaults(func=profile)
    p.add_argument('pdfxml_infile', nargs=1, help="a poppler XML export from a PDF")


def get_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog='xpdf2md')

    # Main
    parser.add_argument(
        '-q', '--quiet',
        help="Be silent",
        action='store_true',
        default=False
    )
    parser.add_argument(
        '-v', '--verbose',
        help="Extra output",
        action='store_true',
        default=False
    )
    subparsers = parser.add_subparsers(help='command')

    _setup_profile_parser(subparsers.add_parser(
        'profile', aliases=['p'], help='create profile'
    ))

    return parser


def main(in_args = None):
    parser = get_argparser()
    arguments = parser.parse_args(in_args)
    if arguments.quiet:
        logger.setLevel(logging.ERROR)
    if arguments.verbose:
        logger.setLevel(logging.DEBUG)
    if not arguments.verbose and not arguments.quiet:
        logger.setLevel(logging.INFO)
    
    logger.debug("XPDF2MD is starting.")
    try:
        func = arguments.func
    except AttributeError:
        logger.error("Nothing to do. Bye.")
        sys.exit(ReturnCode.NO_COMMAND.value)
    try:
        func(arguments)
    except FinalXPDF2MDException as e:
        logger.fatal("Error. Terminating. {}".format(e.msg))
        sys.exit(e.code)

    #document = Document(stat, root, meta)
    #document.write_chapters_to(os.getcwd())


if __name__ == "__main__":
    main()

