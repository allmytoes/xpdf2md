import logging
from dataclasses import dataclass
from typing import Optional, Callable
from xpdf2md.parser import Page, TextElement, FontProfile

logger = logging.getLogger("xpdf2md")

@dataclass
class ValuesStats:
    minimum: int
    maximum: int


class StatTable:
    def __init__(self) -> None:
        self.values: dict[int, int] = {}

    @property
    def values_stats(self) -> ValuesStats:
        return ValuesStats(
            minimum=min(self.values.values()),
maximum=max(self.values.values()),
        )

    def inc(self, key: int):
        if key not in self.values:
            self.values[key] = 0
        self.values[key] += 1

    def get_keys_sorted_by_value(self, filter: Optional[Callable[[ValuesStats, int], bool]] = None) -> list[int]:
        tuples = [ (k, v) for k, v in self.values.items()]
        tuples.sort(key=lambda e: e[1])
        stats = None if filter is None else self.values_stats
        return [ e[0] for e in tuples if filter is None or stats is None or filter(stats, e[1])]

    def get_key_with_highest_value(self) -> int:
        h_v: Optional[int] = None
        h_k: Optional[int] = None
        for k,v in self.values.items():
            if h_v is None or v > h_v:
                h_v = v
                h_k = k
        assert h_k is not None
        return h_k


class PerFontStatTable:
    def __init__(self) -> None:
        self.values: dict[int, StatTable] = {}
    
    def get(self, font_id: int) -> StatTable:
        if font_id not in self.values:
            self.values[font_id] = StatTable()
        return self.values[font_id]

    @property
    def values_with_values(self) -> dict[int, dict[int, int]]:
        return {
            k: v.values
            for k, v in self.values.items()
        }

    def get_fonts_ordered_by_value_for_key(self, key: int) -> list[int]:
        font_x_val: list[ tuple[int, int]] = []
        for font_id, table in self.values.items():
            if key in table.values:
                font_x_val.append((font_id, table.values[key]))
        font_x_val.sort(key=lambda e: e[1])
        return [e[0] for e in font_x_val]

    def __str__(self) -> str:
        result = None
        for k,v in self.values.items():
            if result is None:
                result = ""
            else:
                result += "\n"
            result += "{} :{}".format(k, v.values)
        if result is None:
            return "[]"
        else:
            return result
    


@dataclass
class StatResult:
    bread_font: int
    butter_fonts: list[int]
    chapter_font: int
    ignore_fonts: list[int]
    indentation_marker: Optional[tuple[int, int]]


class SideStatSet:
    def __init__(self) -> None:
        self.font_to_startline_x_to_count = PerFontStatTable()


class Stat:
    def __init__(self, pages: list[Page], font_profiles: dict[int, FontProfile]) -> None:
        self.pages = pages
        self.font_profiles = font_profiles

        self.font_to_first_in_line_count = StatTable()
        self.font_to_line_from_top_to_count = PerFontStatTable()
        self.font_to_line_from_bottom_to_count = PerFontStatTable()
        self.font_to_is_int_to_count = PerFontStatTable()
        self.font_to_same_line_font_to_count = PerFontStatTable()

        self.even_side_set = SideStatSet()
        self.uneven_side_set = SideStatSet()
        self.both_sides_set = SideStatSet()

        self._profile_one()
        self._evaluate()

    def _profile_one(self):
        logger.info("--------------------------------------------")
        logger.info("Profiling starting")
        logger.info("--------------------------------------------")
        for page in self.pages:
            logger.debug("Profiling page {}...".format(page.page_num))
            num_lines = len(page.lines)
            is_even_page = page.page_num % 2
            for line_no, line in enumerate(page.lines):
                first_element = line.elements[0]
                line_no_from_back = num_lines - line_no - 1
                assert line_no_from_back >= 0
                if isinstance(first_element, TextElement):
                    self.font_to_first_in_line_count.inc(first_element.font_id)
                    self.both_sides_set.font_to_startline_x_to_count.get(first_element.font_id).inc(first_element.x)
                    if is_even_page:
                        self.even_side_set.font_to_startline_x_to_count.get(first_element.font_id).inc(first_element.x)
                    else:
                        self.uneven_side_set.font_to_startline_x_to_count.get(first_element.font_id).inc(first_element.x)


                font_ids_in_line = set()
                for element in line.elements:
                    if isinstance(element, TextElement):
                        font_ids_in_line.add(element.font_id)
                        self.font_to_line_from_top_to_count.get(element.font_id).inc(line_no)
                        self.font_to_line_from_bottom_to_count.get(element.font_id).inc(line_no_from_back)
                        try:
                            _ = int(element.text.strip())
                            self.font_to_is_int_to_count.get(element.font_id).inc(1)
                        except ValueError:
                            self.font_to_is_int_to_count.get(element.font_id).inc(0)
                if len(font_ids_in_line) > 1:
                    for font_id in font_ids_in_line:
                        for sibling_font_id in font_ids_in_line:
                            if sibling_font_id != font_id:
                                self.font_to_same_line_font_to_count.get(font_id).inc(sibling_font_id)


    def _get_fonts_with_mostly_numbers(self, thres_rate: float = 0.95) -> list[int]:
        result = []
        for font_id, table in self.font_to_is_int_to_count.values.items():
            v = table.values
            if 1 in v:
                assert v[1] > 0
                if v[1] / (v[1] + v.get(0, 0)) >= thres_rate:
                    result.append(font_id)
        return result

    def _filter_fonts_which_appear_only_in_the_leading_or_tailing_lines(self, font_ids: list[int], thres_pos: int = 2):
        return [
            f for f in font_ids
            if (
                (
                    f in self.font_to_line_from_top_to_count.values
                    and
                    len(self.font_to_line_from_top_to_count.values[f].values) == 1
                    and
                    list(self.font_to_line_from_top_to_count.values[f].values.keys())[0] <= thres_pos
                )
                or
                (
                    f in self.font_to_line_from_bottom_to_count.values
                    and
                    len(self.font_to_line_from_bottom_to_count.values[f].values) == 1
                    and
                    list(self.font_to_line_from_bottom_to_count.values[f].values.keys())[0] <= thres_pos
                )
            )
        ]


    def _evaluate_indentation_paragraph_marker(self, bread_font: int) -> tuple[Optional[int], Optional[int]]:
        min_delta = 10
        min_quota = 0.05
        max_quota = 0.6
        result: list[Optional[int]] = [None, None]
        for startline_map, result_ix, name in [
                (self.even_side_set.font_to_startline_x_to_count, 0, "even"),
                (self.uneven_side_set.font_to_startline_x_to_count, 1, "uneven"),
            ]:
            logger.info("Checking for indentation as paragraph marker for {} pages...".format(
                name
            ))
            assert isinstance(startline_map, PerFontStatTable)

            if len(startline_map.values) < 2:
                logger.warn("Less than two indentations found. No chance to use the identation as paragraph indicator.")
                continue

            # hack for one-page docs (and unit tests 😉)
            if len(self.pages) == 1 and result_ix == 1:
                continue
            
            startline_xs_value_tuples = [(k, v) for k,v in startline_map.values[bread_font].values.items()]
            startline_xs_value_tuples.sort(key=lambda t: t[1])
            logger.debug("  startline_xs values with counts: " + ", ".join(["{} ({})".format(e[0],e[1]) for e in startline_xs_value_tuples]))
            two_most_common = [startline_xs_value_tuples[-1][0], startline_xs_value_tuples[-2][0]]
            upper_xs = max(two_most_common)
            lower_xs = min(two_most_common)
            delta = upper_xs - lower_xs
            if delta > min_delta:
                logger.debug("  Min and max delta is {} which is acceptable as paragraph indicator. Checking quota now...".format(delta))
                middle_xs = round(lower_xs + (upper_xs - lower_xs) / 2)
                logger.debug("    -> upper-xs: {}, lower-xs: {}, middle-xs: {}".format(upper_xs, lower_xs, middle_xs))
                count_less = 0
                count_more = 0
                for xs, count in startline_xs_value_tuples:
                    if xs > middle_xs:
                        count_more += count
                    else:
                        count_less += count
                assert count_more > 0
                assert count_less > 0
                quota = count_more / count_less
                if quota < min_quota:
                    logger.warn("Quota is {:.3f} which is less than the min quota {}. Not accepted as paragraph marker".format(quota, min_quota))
                    logger.warn("Skipping indentation as paragraph marker.")
                elif quota > max_quota:
                    logger.warn("Quota is {:.3f} which is more than the max quota {}. Not accepted as paragraph marker".format(quota, max_quota))
                    logger.warn("Skipping indentation as paragraph marker.")
                else:
                    logger.info("  Quota between indentation levels is {:.3f} which is accepted for paragraph marker with value {}.".format(quota, middle_xs))
                    result[result_ix] = middle_xs

            else:
                logger.warn("Min and max delta is {} which is NOT great enough to be considered as paragraph indicator.".format(delta))
                logger.warn("Skipping indentation as paragraph marker.")

        assert len(result) == 2
        return result[0], result[1]



    def _evaluate(self):
        bread_font = self.font_to_first_in_line_count.get_key_with_highest_value()
        logger.info("Assume font {} as bread font because it's the most common one.".format(bread_font))

        ignore_list = []
        fonts_with_mostly_numbers = self._get_fonts_with_mostly_numbers()
        page_number_candidates = self._filter_fonts_which_appear_only_in_the_leading_or_tailing_lines(fonts_with_mostly_numbers)
        if len(page_number_candidates) == 0:
            logger.warn("Could not identify any font for page numbers. No page numbers will be ignored.")
        elif len(page_number_candidates) == 1:
            logger.info(
                "Assume font {} as page number font and put it on the ignore list because it appears mostly with numbers only on the top or bottom end of the page.".format(
                    page_number_candidates[0]
                )
            )
            ignore_list.append(page_number_candidates[0])
        else:
            logger.warn("Muliple fonts have been identified as page number fonts: {} All will be put on the ignore list. This might be wrong.")
            ignore_list.extend(page_number_candidates)

        if bread_font in self.font_to_same_line_font_to_count.values:
            butter_font_table = self.font_to_same_line_font_to_count.values[bread_font]
            butter_fonts = butter_font_table.get_keys_sorted_by_value()[::-1]
            logger.info("Identified butter fonts {} because they appear with the bread font in common lines.".format(butter_fonts))
            if len(set(butter_fonts).intersection(set(ignore_list))) > 0:
                logger.error(
                    "Some fonts have been identified as butter font which are also on the ignore list: "
                    +
                    str(set(butter_fonts).intersection(set(ignore_list)))
                )
        else:
            butter_fonts = []
            logger.info("No butter fonts identifies because the bread font only appears alone in each line.")

        top_first_line_font = [a for a in self.font_to_line_from_top_to_count.get_fonts_ordered_by_value_for_key(0) if a not in [bread_font] + ignore_list]
        assert len(top_first_line_font) > 0
        chapter_font = top_first_line_font[-1]
        logger.info("Assume font {} as chapter heading font because it's the most common one in the first line of pages beside bread & butter fonds and ignored fonts.".format(chapter_font))

        para_indentation_markers = self._evaluate_indentation_paragraph_marker(bread_font)
        if len(self.pages) == 1:
            if para_indentation_markers[0] is None:
                para_indentation_marker = None 
            else:
                para_indentation_marker = para_indentation_markers[0], para_indentation_markers[0]
        else:
            if (para_indentation_markers[0] is None) != (para_indentation_markers[0] is None):
                logger.warning("A paragraph indentation marker has been found for only one page-side. We can't use that. Withdraw the indentation marker for both. No indentation paragraph marker available.")
                para_indentation_marker = None
            elif para_indentation_markers[0] is None:
                para_indentation_marker = None
            else:
                assert para_indentation_markers[0] is not None
                assert para_indentation_markers[1] is not None
                para_indentation_marker = para_indentation_markers[0], para_indentation_markers[1]
        
        self.result = StatResult(
            bread_font = bread_font,
            butter_fonts=butter_fonts,
            chapter_font=chapter_font,
            ignore_fonts=ignore_list,
            indentation_marker=para_indentation_marker
        )

