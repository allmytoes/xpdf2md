from dataclasses import dataclass
import logging
from typing import Optional, Generator
from xpdf2md.stat import StatResult
from xpdf2md.parser import ImageElement, Page, FontProfile, TextElement, PageElement
from xpdf2md.document import (
    Document,
    SpanStyle,
    ChapterType,
    Chapter,
    Paragraph,
    FencedText,
    Image,
    Span,
)


logger = logging.getLogger("xpdf2md")


@dataclass
class Conf:
    NumBlindChapFromStart = 0
    line_break_hyphens = '-‐–'
    

class Assembler:
    def __init__(self, pages: list[Page], font_profiles: dict[int, FontProfile], stat_result: StatResult) -> None:
        self.pages = pages
        self.font_profiles = font_profiles
        self.stat_result = stat_result
        self.conf = Conf()
        self._doc = Document()
        self.hypen_breaks = []
        logger.info("--------------------------------------------")
        logger.info("Assembling document structure starting")
        logger.info("--------------------------------------------")
        self._doc.chapters.append(Chapter(ChapterType.HEADLESS))
        self._assemble()


        self._current_paragraph: Optional[Paragraph] = None
        self._current_chapter: Optional[Chapter] = None
        self._last_font: Optional[int] = None
        self._queued_font_post: str = ""
        
    def _pull_element(self) -> Generator[tuple[PageElement, int, int, int, int], None, None]:
        """
        :returns: TextElement, line from top, line from bottom, number in line, page number
        """
        for page in self.pages:
            logger.debug("Assembling data from page {}...".format(page.page_num))
            line_num = len(page.lines)
            for line_ix, line in enumerate(page.lines):
                line_bottom_ix = line_num - line_ix
                for ele_ix, ele in enumerate(line.elements):
                    yield (ele, line_ix, line_bottom_ix, ele_ix, page.page_num)

    def _get_style_for_font(self, font_id: int) -> SpanStyle:
        if font_id == self.stat_result.bread_font:
            return SpanStyle.NORMAL
        if len(self.stat_result.butter_fonts) > 0 and font_id == self.stat_result.butter_fonts[0]:
            return SpanStyle.EMPH
        return SpanStyle.CODE

    def _assemble(self):
        last_ele: Optional[PageElement] = None
        last_page: Optional[int] = None
        paragraph_fonts = self.stat_result.butter_fonts + [self.stat_result.bread_font]
        for ele, line_top, line_bot, num_in_line, page_number in self._pull_element():
            logger.debug("Element: {}".format(str(ele)))
            logger.debug("Latest section: {}".format(
                "NONE"
                if len(self._doc.chapters[-1].sections) == 0
                else
                str(self._doc.chapters[-1].sections[-1])
            ))
            page_is_even = page_number % 2 == 0
            if isinstance(ele, TextElement):
                logger.debug("  -> It's a text element ({})".format(ele.text))
                # handling INGORES
                if ele.font_id in self.stat_result.ignore_fonts:
                    logger.debug("    -> Text Element Ignored!")
                    continue
                # handling CHAPTER
                elif ele.font_id == self.stat_result.chapter_font:
                    logger.debug("    -> It's a chapter")
                    if isinstance(last_ele, TextElement) and last_ele.font_id == ele.font_id:
                        self._doc.chapters[-1].title += "\n" + ele.text
                    else:
                        self._doc.chapters.append(Chapter(
                            ChapterType.NORMAL
                            if len(self._doc.chapters) > self.conf.NumBlindChapFromStart
                            else
                            ChapterType.BLIND
                        ))
                        self._doc.chapters[-1].title = ele.text
                # handling normal PARAGRAPH
                elif ele.font_id in paragraph_fonts:
                    logger.debug("    -> Text Element in a paragraph")
                    if last_ele is None or not isinstance(last_ele, TextElement) or last_ele.font_id not in paragraph_fonts:
                        logger.debug("    -> Adding new paragraph because last element was not a normal paragraph")
                        self._doc.chapters[-1].sections.append(
                            Paragraph()
                        )
                        last_font = -1
                    else:
                        last_font = last_ele.font_id
                        if num_in_line == 0 and self.stat_result.indentation_marker is not None:
                            if ele.x > self.stat_result.indentation_marker[1 if page_is_even else 0]:
                                logger.debug("    -> Adding new paragraph because of indentation marker")
                                self._doc.chapters[-1].sections.append(
                                    Paragraph()
                                )

                    p = self._doc.chapters[-1].sections[-1]
                    assert isinstance(p, Paragraph)
                    style = self._get_style_for_font(ele.font_id)
                    if ele.font_id == last_font and isinstance(last_ele, TextElement):
                        if ele.y != last_ele.y:
                            # ! Line break
                            assert last_ele is None or ele.y > last_ele.y or last_page is not None and page_number > last_page
                            if (
                                len(p.spans) > 0
                                and
                                len(p.spans[-1].text) > 0
                                and
                                p.spans[-1].text[-1] in self.conf.line_break_hyphens
                                and
                                ele.text[0].islower()
                            ):
                                # this line was to remove the hyphen, but in many cases,
                                # the hyphen is actually a correct part of the word,
                                # so we better leave it as part of the text.
                                # #p.spans[-1].text = p.spans[-1].text[:-1]
                                self.hypen_breaks.append("Page {p}, Line {l}: {t1} … {t2}".format(
                                    p = page_number,
                                    l = line_top,
                                    t1 = p.spans[-1].text.split(" ")[-1],
                                    t2 = ele.text.split(" ")[0]
                                ))
                            else:
                                p.spans.append(Span(" ", style))
                    p.spans.append(Span(ele.text, style))
                # handling UNKNOWN FORMAT
                else:
                    logger.debug("    -> Text Element in an unknown format")
                    if not isinstance(self._doc.chapters[-1].sections[-1], FencedText):
                        self._doc.chapters[-1].sections.append(FencedText())
                        last_ele = None
                    p = self._doc.chapters[-1].sections[-1]
                    assert isinstance(p, FencedText)
                    if last_ele is None:
                        p.text += ele.text
                    else:
                        assert isinstance(last_ele, TextElement)
                        if ele.y == last_ele.y:
                            p.text += "  " + ele.text
                        else:
                            p.text += "\n" + ele.text
                # TextElement-handling done
            elif isinstance(ele, ImageElement):
                logger.debug("  -> It's an image element")
                i = Image(ele.path)
                self._doc.chapters[-1].sections.append(i)
            else:
                assert False
            last_ele = ele
            last_page = page_number

