import unittest


from xpdf2md.parser import (
    Page,
    TextElement
)


class TestPageConstruction(unittest.TestCase):
    def test_order_of_elements(self):
        p = Page([
            TextElement(1, 2, 0, 0, 0, ""),
            TextElement(2, 2, 0, 0, 0, ""),
            TextElement(2, 3, 0, 0, 0, ""),
            TextElement(1, 3, 0, 0, 0, ""),
            TextElement(1, 4, 0, 0, 0, ""),
            TextElement(2, 1, 0, 0, 0, ""),
            TextElement(1, 1, 0, 0, 0, ""),
            TextElement(2, 4, 0, 0, 0, ""),
        ], 666)
        self.assertEqual(
            [1, 2, 1, 2, 1, 2, 1, 2],
            [item.x for line in p.lines for item in line.elements]
        )
        self.assertEqual(
            [1, 1, 2, 2, 3, 3, 4, 4],
            [item.y for line in p.lines for item in line.elements]
        )
