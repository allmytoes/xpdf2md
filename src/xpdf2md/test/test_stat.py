import unittest


from xpdf2md.stat import (
    Stat
)

from xpdf2md.parser import (
    FontProfile,
    Page,
    TextElement,
)



def get_test_font_dict() -> dict[int, FontProfile]:
    return {
        1: FontProfile(1, 8, "foo_sans", "#000000"),
        2: FontProfile(2, 12, "bar_sans", "#000000"),
        3: FontProfile(3, 16, "baz_sans", "#000000"),
    }

def get_text_pages_list_single_page() -> list[Page]:
    return [
        Page(
            elements=[
                TextElement(1, 1, 66, 66, 3, ""),
                TextElement(1, 2, 66, 66, 1, ""),
                TextElement(1, 3, 66, 66, 1, ""),
                TextElement(2, 3, 66, 66, 1, ""),
                TextElement(1, 4, 66, 66, 2, ""),
            ],
            page_num=1
        )
    ]


class TestPageConstruction(unittest.TestCase):

    def test_font_to_line_from_top_to_count(self):
        stat = Stat(
            pages=get_text_pages_list_single_page(),
            font_profiles=get_test_font_dict(),
        )

        expected = {
            1: {1: 1, 2: 2},
            2: {3: 1},
            3: {0: 1},
        }

        self.assertEqual(expected, stat.font_to_line_from_top_to_count.values_with_values)

    def test_result_chapter_font(self):
        stat = Stat(
            pages=get_text_pages_list_single_page(),
            font_profiles=get_test_font_dict(),
        )
        self.assertEqual(3, stat.result.chapter_font)

