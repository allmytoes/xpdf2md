from enum import Enum, auto
import logging

logger = logging.getLogger("xpdf2md")

class ChapterType(Enum):
    NORMAL = auto()
    BLIND = auto()
    HEADLESS = auto()


class SpanStyle(Enum):
    NORMAL = auto()
    EMPH = auto()
    HEAVYEMPH = auto()
    CODE = auto()


class Document:
    def __init__(self) -> None:
        self.chapters: list[Chapter] = []


class Chapter:
    def __init__(self, chapter_type: ChapterType) -> None:
        self.chapter_type = chapter_type
        self.sections: list[Section] = []
        self.title: str = ""
        logger.info("Constructued new chapter of type {}.".format(
            chapter_type.name
        ))


class Section:
    def __init__(self) -> None:
        pass


class Paragraph(Section):
    def __init__(self) -> None:
        self.spans: list[Span] = []


class FencedText(Section):
    def __init__(self) -> None:
        self.text: str = ""


class Image(Section):
    def __init__(self, url: str) -> None:
        self.url = url


class Span:
    def __init__(self, text: str, style: SpanStyle) -> None:
        self.text = text
        self.style = style

