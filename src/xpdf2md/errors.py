from enum import Enum


class ReturnCode(Enum):
    NO_COMMAND = 1
    XML_PARSE_ERROR = 10
    UNKNOWN = 999


class FinalXPDF2MDException(Exception):
    code: int = ReturnCode.UNKNOWN.value
    def __init__(self, msg: str, *args: object) -> None:
        super().__init__(*args)
        self.msg = msg


class XMLParseError(FinalXPDF2MDException):
    code = ReturnCode.XML_PARSE_ERROR.value
