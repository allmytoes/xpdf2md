import logging
import termcolor

class CustomFormatter(logging.Formatter):

    format_upper = "%(levelname)s: %(message)s"
    format_debug = "%(levelname)s: %(message)s  (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: termcolor.colored(format_debug, "light_grey"),
        logging.INFO: termcolor.colored(format_upper, "blue"),
        logging.WARNING: termcolor.colored(format_upper, "yellow"),
        logging.ERROR: termcolor.colored(format_upper, "magenta"),
        logging.CRITICAL: termcolor.colored(format_upper, "red"),
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)
