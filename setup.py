from distutils.core import setup

setup(
    name='xpdf2md',
    version='0.0.1',
    packages=['xpdf2md'],
    package_dir={'': 'src'},
    url='',
    license='GPL',
    description='',
    install_requires=[
        'pyyaml',
        'termcolor',
        'argparse',
        'lxml',
        'markdownify',
        'pytest'
    ]
)
