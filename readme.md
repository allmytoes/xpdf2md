Converting `pdftohtml -xml -stdout "some-pdf"` output to Markdown.
# ToDo

## ✅ Handle multiple indents for paragraph indication

## ⭕ Bug: First Shark chapter is merged into Intro-Chapter
Not really a bug. Turns out there is a different font used only for the first chapter.
Not sure how to handle that. We could allow muliple chapter fonts, but the additional
ones have to be provided by the user. The SW cannot derive additional chapter fonts from a single outliner.

## ✅ Create Chapter index in mdwriter

## ✅ Name headless, blind, and normal chapters with different prefixes in the output

## ✅ Create an additional index chapter

# Brainstorm
### `profiling`
```mermaid
flowchart LR

    pdf([PDF-XML]) -->
    stat[PDFStat] -->
    profile[PDFProfile] -->
    prof([profile.yaml])
```

### `converting`
```mermaid
flowchart LR

    pdf([PDF-XML])
    prof([profile.yaml])
    scan[Scanner] -->
    doc[Document] -->
    conv[Converter] -->
    mks([Markdowns])

    pdf --> scan
    prof --> scan
```

### `PDFStat`
```mermaid
classDiagram
    direction LR
    class PDFStat {
        
    }
    class FontInstance {
        + id: int
        + number_total
        + number_lines
    }
    PDFStat *--> "*" FontInstance
    class StatTable {
        <<dict[int, int]>>
    }
    FontInstance *--> StatTable : IndentWhenFirstOnLine
    FontInstance *--> StatTable : DistanceFromLastLine
    FontInstance *--> StatTable : AppearsInLineWith
    FontInstance *--> StatTable : AppearsAfterLineStartedWith
    FontInstance *--> StatTable : IndexFirstOnPage
    FontInstance *--> StatTable : IndexFirstOnPageWithNumbersOnly
    FontInstance *--> StatTable : IndexLastOnPage
    FontInstance *--> StatTable : IndexLastOnPageWithNumbersOnly
    FontInstance *--> StatTable : TopWhenFirstOnPage


```


### `Document`
```mermaid
classDiagram

    class Document {
    }

    class Chapter {
        + blind: bool
        + title: str
    }

    class Section {
    }

    class Paragraph {
    }

    class Image {
    }

    Chapter *--> "*" Section
    Section <|-- Paragraph
    Section <|-- Image

    class TitlePage {
    }

    class ImageTitlePage {
    }

    TitlePage <|-- ImageTitlePage

    Document *--> "*" Chapter 
    Document *--> "0,1" TitlePage
```
